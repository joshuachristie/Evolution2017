function UB_2n = meiosis_double_UB(UB,prob_matrix_meiosis,n)

UB_2n = zeros(1,2*n+1);

for ii = 0:n
    for jj = 0:n
        UB_2n(ii+jj+1) = UB_2n(ii+jj+1) + prob_matrix_meiosis(ii+1,jj+1)*UB(ii+1);
    end
end
end