function BB_halfn = meiosis_gametes_BB(BB_2n,prob_matrix_gametes,n)

BB_halfn = zeros(1,n/2+1);

for ii = 0:2*n
    for jj = 0:n/2
        BB_halfn(jj+1) = BB_halfn(jj+1) + prob_matrix_gametes(ii+1,jj+1)*BB_2n(ii+1);
    end
end

end





