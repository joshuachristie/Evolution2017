function W = fitness_concave_advantageous(k,n)
W = zeros(1,n+1);

for ii = 0:n
    W(ii+1) = 1 - k + (k/sqrt(n))*sqrt(ii);
end
   
end
