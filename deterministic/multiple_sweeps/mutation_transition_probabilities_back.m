%Gives the probability that a cell type with ii advantageous mutations (and
%n-ii wild type mutations) accumulates jj mutations

%premutation cell has ii mutations while postmutation cell has ii + jj
%mutations

%let ii be the number of adv mitochondria pre mutation
%let aa be the number of mutations in w.t. mitochondria
%let bb be the number of mutations in adv mitochondria
%so ii+aa-bb is number of adv mitochondria post mutation
function mutation_prob_matrix = mutation_transition_probabilities_back(n,u,u_b)

mutation_prob_matrix = zeros(n+1,n+1);

for ii = 0:n
    for aa = 0:n-ii
        for bb = 0:ii
            mutation_prob_matrix(ii+1,ii+aa-bb+1) = mutation_prob_matrix(ii+1,ii+aa-bb+1) + binopdf(aa,n-ii,u)*binopdf(bb,ii,u_b);
        end
    end
end

end


