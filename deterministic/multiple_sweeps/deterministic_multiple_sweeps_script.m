% @2017 by Joshua Christie (joshua.christie@ieu.uzh.ch)

clear;

%PARAMETERS

% number of mitochondria per cell (must be even)
n = 50;
%  selection coefficient (sb in manuscript)
k = 0.5;
% choose a fitness function (1 for concave, 2 for linear, or 3 for convex)
fit_func = 1;
%frequency at which U1 is introduced
U_intro = 0.01;
% set tol for BB equi
tol = 1e-12;
%set tol for final equi
tol_1 = 1e-12;
%tol for fixation
tol_2 = 1e-4;
%mutation
%forward
u = 1e-6;
%back
u_b = u;
% location of output directory (default is current directory)
output_dir = pwd;

%TRANSITION PROBABILITIES

%probability matrix for sampling n mito with replacement from n mito
prob_matrix_meiosis = meiosis_doubling(n);

%probability matrix for sampling n/2 mito without replacement from 2n mito
prob_matrix_gametes = meiosis_nover2(n);

%probability matrix for sampling n/2 mito with replacement from n/2 mito
prob_matrix_UB = UB_replace_matrix(n);

%mutation transition probabilities
mutation_prob_matrix = mutation_transition_probabilities_back(n,u,u_b);

%fluctuating selection parameters
number_switches = 1000;
gen_before_switch = 1000;

% initialise vectors to store output
UBvec = [];
UBmat=[];
U1cell={};
B2cell={};
BBmat=[];

%fitness function
if fit_func == 1
    W = fitness_concave_advantageous(k,n);
elseif fit_func == 2
    W = fitness_linear_advantageous(k,n);
else
    W = fitness_convex_advantageous(k,n);
end

%initialize with haploid B1/B2 gametes
B1 = zeros(1,n/2+1);
B2 = zeros(1,n/2+1);
B1(1) = 0.5;
B2(1) = 0.5;
UB = zeros(1,n+1);

%random mating
BB = random_mating_advant_BB(B1,B2,n);

%normalise
total_sum = sum(UB) + sum(BB);
BB = BB/total_sum;
UB = UB/total_sum;

%mutation (UB has zero frequency at this stage)
[~,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix);

%fitness
BB = fitness_BB(W,BB,n);

%BB cells go from n mitochondria to 2n mitochondria
BB_2n = meiosis_double_BB(BB,prob_matrix_meiosis,n);

%BB cells with 2n mitochondria produce gametes with n/2 mitochondria
BB_halfn = meiosis_gametes_BB(BB_2n,prob_matrix_gametes,n);

B1 = 0.5*BB_halfn;
B2 = 0.5*BB_halfn;

% U introduced into homoplasmic at this stage

%normalise gametes so that they sum to 1
total_gamete_sum = sum(B1) + sum(B2);
B1 = B1/total_gamete_sum;
B2 = B2/total_gamete_sum;

U1 = zeros(1,n/2+1);
U1(1) = U_intro;
B1(1) = B1(1) - U_intro;

%random mating
BB = random_mating_advant_BB(B1,B2,n);
UB = random_mating_advant_UB(U1,prob_matrix_UB,n);

%normalise
total_sum = sum(UB) + sum(BB);
BB = BB/total_sum;
UB = UB/total_sum;

%mutation
[UB,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix);

%fitness
BB = fitness_BB(W,BB,n);
UB = fitness_UB(W,UB,n);

%cells go from n mitochondria to 2n mitochondria
BB_2n = meiosis_double_BB(BB,prob_matrix_meiosis,n);
UB_2n = meiosis_double_UB(UB,prob_matrix_meiosis,n);

%cells with 2n mitochondria produce gametes with n/2 mitochondria
BB_halfn = meiosis_gametes_BB(BB_2n,prob_matrix_gametes,n);
UB_halfn = meiosis_gametes_UB(UB_2n,prob_matrix_gametes,n);

U1 = 0.5*UB_halfn;
B1 = 0.5*BB_halfn;
B2 = 0.5*BB_halfn + 0.5*UB_halfn;

counter = 1;

% record information about the simulation
UBvec(counter) = sum(UB);
UBmat(counter,:) = UB;
U1cell{counter} = U1;
B2cell{counter} = B2;
BBmat(counter,:) = BB;

% alter the fitness functions depending on direction of selection
for switch_loop = 1:number_switches
    
    if mod(switch_loop,2) == 0
        if fit_func == 1
            W = fitness_concave_advantageous(k,n);
        elseif fit_func == 2
            W = fitness_linear_advantageous(k,n);
        else
            W = fitness_convex_advantageous(k,n);
        end
        
        
    else
        if fit_func == 1
            W = fitness_concave_advantageous_flipped(k,n);
        elseif fit_func == 2
            W = fitness_linear_advantageous_flipped(k,n);
        else
            W = fitness_convex_advantageous_flipped(k,n);
        end
        
    end
    
    for firstloop = 1:gen_before_switch
        
        
        %normalise gametes
        total_gamete_sum = sum(B1) + sum(B2) + sum(U1);
        B1 = B1/total_gamete_sum;
        B2 = B2/total_gamete_sum;
        U1 = U1/total_gamete_sum;
        
        %random mating
        BB = random_mating_advant_BB(B1,B2,n);
        UB = random_mating_advant_UB(U1,prob_matrix_UB,n);
        
        %normalise
        total_sum = sum(UB) + sum(BB);
        BB = BB/total_sum;
        UB = UB/total_sum;
        
        counter = counter + 1;
        
        % record information about the simulation
        UBvec(counter) = sum(UB);
        UBmat(counter,:)=UB;
        U1cell{counter}=U1;
        B2cell{counter}=B2;
        BBmat(counter,:)=BB;
        B1cell{counter} = B1;
        
        %mutation
        [UB,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix);
        
        %fitness
        BB = fitness_BB(W,BB,n);
        UB = fitness_UB(W,UB,n);
        
        %cells go from n mitochondria to 2n mitochondria
        BB_2n = meiosis_double_BB(BB,prob_matrix_meiosis,n);
        UB_2n = meiosis_double_UB(UB,prob_matrix_meiosis,n);
        
        %cells with 2n mitochondria produce gametes with n/2 mitochondria
        BB_halfn = meiosis_gametes_BB(BB_2n,prob_matrix_gametes,n);
        UB_halfn = meiosis_gametes_UB(UB_2n,prob_matrix_gametes,n);
        
        U1 = 0.5*UB_halfn;
        B1 = 0.5*BB_halfn;
        B2 = 0.5*BB_halfn + 0.5*UB_halfn;
        
        %normalise
        total_sum = sum(UB) + sum(BB);
        BB = BB/total_sum;
        UB = UB/total_sum;
        
        % stop if UB becomes fixed
        if abs(sum(UB) - 1) < tol_2
            break
        end
        
    end
    
    %normalise
    total_sum = sum(UB) + sum(BB);
    BB = BB/total_sum;
    UB = UB/total_sum;
    
    % stop if UB becomes fixed
    if abs(sum(UB) - 1) < tol_2
        break
    end
    
end


if u == 1e-3
    u_file = 3;
elseif u == 1e-6
    u_file = 6;
else
    u_file = 9;
end

filename = sprintf('%s/deterministic_multiple_n%d_k%d_f%d_u%d.mat',output_dir,n,k*10,fit_func,u_file);

save(filename);