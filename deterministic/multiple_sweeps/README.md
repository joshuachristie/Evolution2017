The script should work out of the box so long as you ensure that all files are in the same folder.

To run from the MATLAB IDE, simply type the following:

run('path_to_directory_containing_files/deterministic_multiple_sweeps_script.m')

where you should replace path_to_directory_containing_files with the actual path.

For example, if you placed the multiple_sweeps folder in your Downloads file then you should type (on linux/mac)

run('~/Downloads/multiple_sweeps/deterministic_multiple_sweeps_script.m')

Be aware that this script can, depending on the parameter values, run for quite some time  and generate large output files, since simulations can run for up to 1 million generations with the default settings.
