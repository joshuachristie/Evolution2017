function BB = fitness_BB(W,BB,n)
BB_temp = BB;
BB = zeros(1,n+1);

for ii = 0:n
    BB(ii+1) = BB_temp(ii+1)*W(ii+1);
end

end

