function W = fitness_linear_advantageous_flipped(k,n)
W = zeros(1,n+1);

for ii = 0:n
    W(n+1-ii) = 1 - k + (k/(n))*(ii);
end

end
