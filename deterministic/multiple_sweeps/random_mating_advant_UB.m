function UB = random_mating_advant_UB(U1,prob_matrix_UB,n)

UB = zeros(1,n+1);

for ii = 0:n/2
    for jj = 0:n/2
        UB(ii+jj+1) = UB(ii+jj+1) + 2*(U1(ii+1)*prob_matrix_UB(ii+1,jj+1)*0.5); %0.5 is sum of B2 when pop is normalised
    end
end
end


