function W = fitness_convex_advantageous_flipped(k,n)
W = zeros(1,n+1);

for ii = 0:n
    W(n+1-ii) = 1 - k + (k/(n)^2)*(ii)^2;
end
 
end
