function W = fitness_linear_advantageous(k,n)
W = zeros(1,n+1);

for ii = 0:n
    W(ii+1) = 1 - k + (k/(n))*(ii);
end
    
end
