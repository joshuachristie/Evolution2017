function prob_matrix_gametes = meiosis_nover2(n)

prob_matrix_gametes = zeros(2*n+1,n/2+1);

for ii = 0:2*n
    for jj = 0:n/2       
        prob_matrix_gametes(ii+1,jj+1) = hygepdf(jj,2*n,ii,n/2);        
    end
end

end