function [UB,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix)

UB_temp = UB;
BB_temp = BB;
UB = zeros(1,n+1);
BB = zeros(1,n+1);

for ii = 0:n
    for jj = 0:n
        BB(jj+1) = BB(jj+1) + BB_temp(ii+1)*mutation_prob_matrix(ii+1,jj+1);
        UB(jj+1) = UB(jj+1) + UB_temp(ii+1)*mutation_prob_matrix(ii+1,jj+1);
    end
end

end