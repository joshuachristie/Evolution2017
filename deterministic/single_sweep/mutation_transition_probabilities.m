%Gives the probability that a cell type with ii advantageous mutations (and
%n-ii wild type mutations) accumulates jj mutations

%premutation cell has ii mutations while postmutation cell has ii + jj
%mutations
function mutation_prob_matrix = mutation_transition_probabilities(n,u)

mutation_prob_matrix = zeros(n+1,n+1);

for ii = 0:n
    for jj = 0:n-ii        
        mutation_prob_matrix(ii+1,ii+jj+1) = mutation_prob_matrix(ii+1,ii+jj+1) + binopdf(jj,n-ii,u);      
    end
end

end
        
        
    