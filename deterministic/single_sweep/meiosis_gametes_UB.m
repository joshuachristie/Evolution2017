function UB_halfn = meiosis_gametes_UB(UB_2n,prob_matrix_gametes,n)

UB_halfn = zeros(1,n/2+1);

for ii = 0:2*n
    for jj = 0:n/2
        UB_halfn(jj+1) = UB_halfn(jj+1) + prob_matrix_gametes(ii+1,jj+1)*UB_2n(ii+1);
    end
end

end





