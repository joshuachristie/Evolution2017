function prob_matrix_meiosis = meiosis_doubling(n)

prob_matrix_meiosis = zeros(n+1,n+1);

for ii = 0:n
    for jj = 0:n     
        prob_matrix_meiosis(ii+1,jj+1) = binopdf(jj,n,ii/n);        
    end
end

end