function UB = fitness_UB(W,UB,n)
UB_temp = UB;
UB = zeros(1,n+1);

for ii = 0:n
    UB(ii+1) = UB_temp(ii+1)*W(ii+1);
end

end

