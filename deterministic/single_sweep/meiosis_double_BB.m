function BB_2n = meiosis_double_BB(BB,prob_matrix_meiosis,n)

BB_2n = zeros(1,2*n+1);

for ii = 0:n
    for jj = 0:n
        BB_2n(ii+jj+1) = BB_2n(ii+jj+1) + prob_matrix_meiosis(ii+1,jj+1)*BB(ii+1);
    end
end
end