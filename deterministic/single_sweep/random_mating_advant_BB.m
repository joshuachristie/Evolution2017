function BB = random_mating_advant_BB(B1,B2,n)

BB = zeros(1,n+1);

for ii = 0:n/2
    for jj = 0:n/2        
        BB(ii+jj+1) = BB(ii+jj+1) + 2*(B1(ii+1)*B2(jj+1));      
    end
end

end

