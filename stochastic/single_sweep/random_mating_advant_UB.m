function UB = random_mating_advant_UB(U1,prob_matrix_UB,n,N)

P = zeros(1,n+1);

for ii = 0:n/2
    for jj = 0:n/2
        P(ii+jj+1) = P(ii+jj+1) + 2*((U1(ii+1)/(2*N))*prob_matrix_UB(ii+1,jj+1)*0.5); %0.5 gives proportion of B2 since N out of 2N gametes are B2
    end
end

%normalise
P = P/sum(P);
%sample UB cells
UB = mnrnd(sum(U1),P);

end


