The script should work out of the box so long as you ensure that all files are in the same folder.

To run from the MATLAB IDE, simply type the following:

run('path_to_directory_containing_files/stochastic_single_sweep_script.m')

where you should replace path_to_directory_containing_files with the actual path.

For example, if you placed the single_sweep folder in your Downloads file then you should type (on linux/mac)

run('~/Downloads/single_sweep/stochastic_single_sweep_script.m')
