function W = fitness_convex_heteroplasmy(k,n)
tempv=0:n/2; tempv1 = n/2-1:-1:0;

yy= [tempv tempv1];
W = abs(1-k*sqrt((yy./(n/2))));

    %W = diag(W,0);
end
