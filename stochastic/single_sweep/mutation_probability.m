%[prob_matrix_mutation] = prob_matrix_mutation(n,u,u_b)

% a is the # of mutations away from wild type (n-i)
%b is the # of mutations away from mutant type (i)
%u is the mutation rate in wild type mitochondria (which then become mutant)
%and u_b is the mutation rate in the mutant mitochondria (which become
%w.t.)
%if post mutation cells contain i mutant mitochondria then pre-mutation cells
%must contain n-i-b+a wild type mitochondria (n-i w.t. + a that
%were w.t. but become mutant - b  that were mutant but become w.t.)
%and i-a+b mutant mitochondria (i w.t. - a that were w.t. but become mutant
%+ b that were mutant but become w.t.)

function mutation_prob_matrix = mutation_probability(n,u,u_b)

mutation_prob_matrix = zeros(n+1,n+1);

for i = 0:n
    for a = 0:i
        for b = 0:n-i
            
            mutation_prob_matrix(i-a+b+1,i+1) =  mutation_prob_matrix(i-a+b+1,i+1) + binopdf(a,n-i-b+a,u)*binopdf(b,i-a+b,u_b);
            
        end
    end
end


end

