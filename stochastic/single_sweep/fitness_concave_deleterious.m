function W = fitness_concave_deleterious(k,n)
W = zeros(1,n+1);

for ii = 0:n
    W(ii+1) = 1 - (k/(n)^2)*(ii)^2;
end
    %W = diag(W);
end
