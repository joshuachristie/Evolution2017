function [BB_2n,UB_2n] = meiosis_double(UB,BB,prob_matrix_meiosis,n,N)

P_BB = zeros(1,2*n+1);
P_UB = zeros(1,2*n+1);

for ii = 0:n
    for jj = 0:n
        P_BB(ii+jj+1) = P_BB(ii+jj+1) + prob_matrix_meiosis(ii+1,jj+1)*(BB(ii+1)/N);
        P_UB(ii+jj+1) = P_UB(ii+jj+1) + prob_matrix_meiosis(ii+1,jj+1)*(UB(ii+1)/N);
    end
end

P_BB = P_BB/sum(P_BB);
P_UB = P_UB/sum(P_UB);

BB_2n = mnrnd(sum(BB),P_BB);
UB_2n = mnrnd(sum(UB),P_UB);

end