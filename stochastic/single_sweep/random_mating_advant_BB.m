function BB = random_mating_advant_BB(B1,B2,n,N)

P = zeros(1,n+1);

for ii = 0:n/2
    for jj = 0:n/2
        P(ii+jj+1) = P(ii+jj+1) + 2*((B1(ii+1)/(2*N))*(B2(jj+1)/(2*N)));
    end
end
%normalise
P = P/sum(P);
%sample BB cells
BB = mnrnd(sum(B1),P);

end