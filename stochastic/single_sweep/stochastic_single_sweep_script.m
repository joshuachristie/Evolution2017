% @2017 by Joshua Christie (joshua.christie@ieu.uzh.ch)

clear;

% set output directory (default is working directory)
output_directory = pwd;
%PARAMETERS

% number of mitochondria per cell (must be even)
n = 50;
%mutation
u = 1e-6;
%number of cells in the population
N = 1000;
% fitness function (1: concave, beneficial; 2: linear, beneficial; 3:
% convex, beneficial; 4: concave, deleterious; 5: linear, heteroplasmy; 6:
% concave, heteroplasmy; 7: convex, heteroplasmy)
fit_func = 2;
% selection coefficient (sb in manuscript)
k = 0.1;

%back mutation rate (for heteroplasmy only)
u_b = u;

% number of gametes to mutate to U1
inoculum = 1;

%TRANSITION PROBABILITIES

%probability matrix for sampling n mito with replacement from n mito
prob_matrix_meiosis = meiosis_doubling(n);

%probability matrix for sampling n/2 mito without replacement from 2n mito
prob_matrix_gametes = meiosis_nover2(n);

%probability matrix for sampling n/2 mito with replacement from n/2 mito
prob_matrix_UB = UB_replace_matrix(n);

%mutation transition probabilities
if fit_func > 4 %heteroplasmy
    mutation_prob_matrix = mutation_probability(n,u,u_b);
else
    mutation_prob_matrix = mutation_transition_probabilities(n,u);
end

%initialise vectors to store info
num_trials = 100000;
BBcell = {};
UBcell = {};
BB_index = [];
UB_index = [];
gen = zeros(1,num_trials);

%fixation counter
fixation = 0;

for num_trial_loop = 1:num_trials

    % fitness function
    if fit_func == 1
        W = fitness_concave_advantageous(k,n);
    elseif fit_func == 2
        W = fitness_linear_advantageous(k,n);
    elseif fit_func == 3
        W = fitness_convex_advantageous(k,n);
    elseif fit_func == 4
        W = fitness_concave_deleterious(k,n);
    elseif fit_func == 5
        W = fitness_linear_heteroplasmy(k,n);
    elseif fit_func == 6
        W = fitness_concave_heteroplasmy(k,n);
    elseif fit_func == 7
        W = fitness_convex_heteroplasmy(k,n);
    end
    
    % how many generations before U1 is introduced?
    pre_U1_generations = 1;
    
    %initialize with haploid B1/B2 gametes
    B1 = zeros(1,n/2+1);
    B2 = zeros(1,n/2+1);
    
    % initialize starting conditions
    B1(1) = N;
    B2(1) = N;
    
    % set UB to zeros so as not to break the functions
    UB = zeros(1,n+1);
    
    for pre_U1_loop = 1:pre_U1_generations
        
        %random mating
        BB = random_mating_advant_BB(B1,B2,n,N);
        
        %mutation
        [~,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix,N);
        
        %fitness
        [~,BB] = fitness_UB_BB(W,UB,BB,n,N);
        
        %cells go from n mitochondria to 2n mitochondria
        [BB_2n,UB_2n] = meiosis_double(UB,BB,prob_matrix_meiosis,n,N);
        
        %cells with 2n mitochondria produce gametes with n/2 mitochondria
        [B1,B2,~] = meiosis_gametes(BB_2n,UB_2n,prob_matrix_gametes,n,N);
        
    end
    
    % INTRODUCE U1
    
    % choose which B1 gamete mutates into the U1 gamete
    start_pos = randsample(1:(n/2 + 1),1,true,B1/sum(B1));
    
    % introduce U1 gamete
    U1 = zeros(1,n/2+1);
    U1(start_pos) = inoculum;
    B1(start_pos) = B1(start_pos) - inoculum;
    
    counter = 1;
    
    while sum(U1) ~= 0 && sum(U1) ~= N
        
        %random mating
        BB = random_mating_advant_BB(B1,B2,n,N);
        UB = random_mating_advant_UB(U1,prob_matrix_UB,n,N);
        
        %mutation
        [UB,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix,N);
        
        %selection
        [UB,BB] = fitness_UB_BB(W,UB,BB,n,N);
        
        if sum(UB) == N % BB went extinct during selection
            
            % cells go from n mitochondria to 2n mitochondria 
            [~,UB_2n] = meiosis_double(UB,BB,prob_matrix_meiosis,n,N);
            BB_2n = zeros(1,2*n+1);
            
            %cells with 2n mitochondria produce gametes with n/2 mitochondria
            [~,~,U1] = meiosis_gametes(BB_2n,UB_2n,prob_matrix_gametes,n,N);
            B1 = zeros(1,n/2+1);
            B2 = zeros(1,n/2+1); % not strictly true, but it doesn't matter
            % since the if loop will exit (nor is B2 relevant when UPI is
            % the only mode)
            
        elseif sum(BB) == N % UB went extinct during selection
            % cells go from n mitochondria to 2n mitochondria
            [BB_2n,~] = meiosis_double(UB,BB,prob_matrix_meiosis,n,N);
            UB_2n = zeros(1,2*n+1);
            %BB cells with 2n mitochondria produce gametes with n/2 mitochondria
            [B1,B2,~] = meiosis_gametes(BB_2n,UB_2n,prob_matrix_gametes,n,N);
            U1 = zeros(1,n/2+1);
        else % both BB and UB present
            %cells go from n mitochondria to 2n mitochondria
            [BB_2n,UB_2n] = meiosis_double(UB,BB,prob_matrix_meiosis,n,N);
            
            %cells with 2n mitochondria produce gametes with n/2 mitochondria
            [B1,B2,U1] = meiosis_gametes(BB_2n,UB_2n,prob_matrix_gametes,n,N);
        end
               
        counter = counter + 1;
        
    end
    
    BBcell{num_trial_loop} = BB;
    UBcell{num_trial_loop} = UB;
    if sum(U1) == N %B1 is extinct and fixation has occured
        fixation = fixation + 1;
        UB_index = [UB_index,num_trial_loop];
    else
        BB_index = [BB_index,num_trial_loop];
    end
    
    gen(num_trial_loop) = counter;
    
end

switch u
    case 1e-2
        u_short = 12;
    case 5e-3
        u_short = 53;
    case 1e-3
        u_short = 13;
    case 5e-4
        u_short = 54;
    case 1e-4
        u_short = 14;
    case 5e-5
        u_short = 55;
    case 1e-5
        u_short = 15;
    case 5e-6
        u_short = 56;
    case 1e-6
        u_short = 16;
    case 5e-7
        u_short = 57;
    otherwise
        u_short = 666;
end

output_filename = sprintf('%s/stochastic_single_sweep_N%d_n%d_k%2.0f_fit%d_u%d.mat', ...
    output_directory,N,n,k*100, ...
    fit_func,u_short);

% save workspace
save(output_filename)


