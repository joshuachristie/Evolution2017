function [UB,BB] = fitness_UB_BB(W,UB,BB,n,N)
BB_temp = BB;
UB_temp = UB;

P_BB = zeros(1,n+1);
P_UB = zeros(1,n+1);

for ii = 0:n
    P_BB(ii+1) = BB_temp(ii+1)*W(ii+1);
    P_UB(ii+1) = UB_temp(ii+1)*W(ii+1);
end

if any(isnan(P_UB))
mean_fitness = sum(P_BB);

P_BB = P_BB/mean_fitness;

BB = mnrnd(N,P_BB);

else
    mean_fitness = sum(P_BB) + sum(P_UB);

P_BB = P_BB/mean_fitness;
P_UB = P_UB/mean_fitness;

P = [P_BB,P_UB];

BB_UB = mnrnd(N,P);

BB = BB_UB(1:n+1);

UB = BB_UB(n+2:length(BB_UB));
end

end

