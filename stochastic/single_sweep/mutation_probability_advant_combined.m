function [UB,BB] = mutation_probability_advant_combined(n,BB,UB,mutation_prob_matrix,N)

UB_temp = UB;
BB_temp = BB;
P_UB = zeros(1,n+1);
P_BB = zeros(1,n+1);

for ii = 0:n
    for jj = 0:n
        P_BB(jj+1) = P_BB(jj+1) + (BB_temp(ii+1)/N)*mutation_prob_matrix(ii+1,jj+1);
        P_UB(jj+1) = P_UB(jj+1) + (UB_temp(ii+1)/N)*mutation_prob_matrix(ii+1,jj+1);
    end
end
%normalise
P_BB = P_BB/sum(P_BB);
P_UB = P_UB/sum(P_UB);
%choose BB and UB cells
BB = mnrnd(sum(BB),P_BB);
UB = mnrnd(sum(UB),P_UB);
end