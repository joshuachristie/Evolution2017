function prob_matrix_UB = UB_replace_matrix(n)

prob_matrix_UB = zeros(n/2+1,n/2+1);

for ii = 0:n/2
    for jj = 0:n/2    
        prob_matrix_UB(ii+1,jj+1) = binopdf(jj,n/2,ii/(n/2));      
    end
end

end
