function [B1,B2,U1] = meiosis_gametes(BB_2n,UB_2n,prob_matrix_gametes,n,N)

P_BB = zeros(1,n/2+1);
P_UB = zeros(1,n/2+1);

for ii = 0:2*n
    for jj = 0:n/2
        P_BB(jj+1) = P_BB(jj+1) + prob_matrix_gametes(ii+1,jj+1)*(BB_2n(ii+1)/N);
        P_UB(jj+1) = P_UB(jj+1) + prob_matrix_gametes(ii+1,jj+1)*(UB_2n(ii+1)/N);
    end
end

P_BB = P_BB/sum(P_BB);
P_UB = P_UB/sum(P_UB);
%B1 and U1
B1 = mnrnd(sum(BB_2n),P_BB);
U1 = mnrnd(sum(UB_2n),P_UB);
%B2
B2_BB = mnrnd(sum(BB_2n),P_BB);
B2_UB = mnrnd(sum(UB_2n),P_UB);

if sum(isnan(B2_UB)) > 0
    B2 = B2_BB;
else
B2 = B2_BB + B2_UB;
end

end

