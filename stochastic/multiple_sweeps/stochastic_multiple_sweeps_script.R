##  @2017 by Joshua Christie (joshua.christie@ieu.uzh.ch)

## clear all
rm(list=ls(all=TRUE))
# get working directory
working_directory <- getwd()

function_file <- sprintf("%s/functions_multi_locus.R",
                                 working_directory)

## output directory path (default set to your working directory)
output_directory <- getwd()
## call functions file
source(function_file)

## LOAD PACKAGES
require("doMC") ## note that doMC doesn't work on windows: you need to use doSNOW instead
require("doParallel")
require("foreach")
mcoptions <- list(preschedule=FALSE)

## MODEL PARAMETERS
## number of diploid cells in the population
N <- 1000
## number of mitochondria per cell
n <- 50
## number loci per mitochondrion
l <- 20000
# this needs to stay as s=1 (it's for backwards compatibility with the fitness function in the model in Christie and Beekman (2017))
s <- 1
## maximum mutations per loci for fitness function (given by s*fit_cap)
fit_cap <- 1000 ## much larger than it needs to be be but this ensures that no simulation will go beyond these bounds (75 mutations per mitochondrion)
## advantage conferred by the advantageous mutant
k <- 0.1
## number of MC simulations
num_trials <- 100000
## choose a fitness function (1 for concave (concave up, increasing), 2 for linear, 3 for convex (concave down, increasing)
fit_func <- 2
## choose rate for advantageous mutation (no back mutation)
u <- 1e-8
## number of mitochondria in gametes (bottleneck)
bot <- n/2
## set lower limit for the number of generations before U1 is introduced
pre_U1_lower_limit <- 100
## set range of the number of generations before U1 is introduced (upper limit is pre_U1_lower_limit + pre_U1_upper_range)
pre_U1_upper_range <- 100

## HPC PARAMETERS
## number of cores
num_cores <- 2

## choose fitness function
if (fit_func == 1){
    W <- fitness_concave(n,s,k,fit_cap)
} else if (fit_func == 2) {
    W <- fitness_linear(n,s,k,fit_cap)
} else {
    W <- fitness_convex(n,s,k,fit_cap)
}

## mutation probability matrix
mutation_prob_matrix <- mutation_probability(l,u,s,fit_cap)

registerDoMC(cores = num_cores)

## LOOP OVER INDEPENDENT MONTE CARLO SIMULATIONS
results <- foreach (loop = 1:num_trials, .options.multicore=mcoptions, .combine = rbind) %dopar% { 
    
    ## how many generations before U1 is introduced?
    pre_U1_generations <- round((runif(1) * pre_U1_upper_range)) + pre_U1_lower_limit

    ## counter for generations once U1 is introduced
    counter <- 1
    
    temp_list <- list()
    
    B1 <- matrix(numeric(N*bot),nrow=N,ncol=bot)
    B2 <- matrix(numeric(N*bot),nrow=N,ncol=bot)

    ## run through generations with only B1/B2 gametes (BB cells) for a random (pre_U1_generations) number of generations
    for (pre_U1_loop in 1:pre_U1_generations) {

        ## random mating (no U1)
        population <- randmating_B1B2(N,n,bot,B1,B2)

        ## mutation
        population <- mutation(n,population,N,mutation_prob_matrix)
        
        ## fitness
        population <- fitness(N,n,W,population)

        ## meiosis (no U1)
        gametes <- meiosis_B1B2(n,bot,N,population)
        
        B1 <- matrix(unlist(gametes[1]), nrow = N, ncol = bot)
        B2 <- matrix(unlist(gametes[2]), nrow = N, ncol = bot)
        
    }

    ## INTRODUCE U1

    ## a single B1 gamete becomes a U1 gamete (each gamete has an equal chance)
    U1_index <- sample(1:nrow(B1), 1)

    ## what is the number of beneficial mutations in that B1 gamete relative to other B1 gametes?
    U1_introduction_conditions <- c(sum(B1[U1_index,]), mean(rowSums(B1)))

    ## initialise U1
    U1 <- matrix(B1[U1_index, ], nrow=1, ncol=bot)

    ## remove the B1 gamete that become the U1 gamete
    B1 <- B1[-U1_index, ]
    
    ## LOOP UNTIL U1 IS FIXED OR LOST
    
    while (U1 != "U1_lost" && !is.null(U1) && nrow(U1) != N){ ## neither fixed nor lost

        ## random mating
        population <- randmating_U1B1B2(N,n,bot,U1,B1,B2)

        if (counter == 1) { ## initialization
            population_after_U1_introduction <- population
        } 

        ## mutation
        population <- mutation(n,population,N,mutation_prob_matrix)
        
        ## fitness
        population <- fitness(N,n,W,population)
        
        ## meiosis
        if (sum(population[ , n+1] == 0) == 1000) { ## no UB cells
            ## once it reaches the end of this loop, it will exist as nrow(U1) == 0
            gametes <- meiosis_B1B2(n,bot,N,population)
            
            U1 <- "U1_lost"
            B1 <- matrix(unlist(gametes[1]), nrow = N, ncol = bot)
            B2 <- matrix(unlist(gametes[2]), nrow = N, ncol = bot)

        } else if (sum(population[ , n+1] == 0) == 0) { ## no BB cells

          U1 <- meiosis_U1B2(n,bot,N,population)

        } else { ## both UB and BB cells
          
          gametes <- meiosis_U1B1B2(n,bot,N,population)
          
          U1 <- matrix(unlist(gametes[1]), nrow = unlist(gametes[4]), ncol = bot)
          B1 <- matrix(unlist(gametes[2]), nrow = unlist(gametes[5]), ncol = bot)
          B2 <- matrix(unlist(gametes[3]), nrow = unlist(gametes[6]), ncol = bot)
          
        }

        counter <- counter + 1
        
    }

    ## is U1 fixed or lost?
    if (U1 == "U1_lost") { 
      fixation <- 0
    } else if (nrow(U1) == N) {
      fixation <- 1
    } else {
      fixation <- "error in fixation"
    }

    
    temp_list[[1]] <- counter
    temp_list[[2]] <- pre_U1_generations
    temp_list[[3]] <- fixation
    temp_list[[4]] <- U1_introduction_conditions
        
    results <- temp_list
}

if (u == 1e-8){
    u_short <- 8
} else if (u == 1e-9) {
    u_short <- 9
} else {
    u_short <- 666
}

filename <- sprintf("%s/stochastic_multiple_sweeps_N%d_n%d_u%d_k%1.2f_fit%d_up%d.RData",output_directory, N,n,u_short,k,fit_func,pre_U1_upper_range)
save(results, file = filename)
