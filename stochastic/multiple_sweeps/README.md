This script will only work out of the box if you run osx/linux, have already installed the required packages, and you ensure that the script and function files are in the same folder.

If you don't have the installed packages (doMC, foreach, doParallel), you can install them with:

install.packages('name of package')

For example, to install the package 'doMC' run the following in the R console:

install.packages('doMC')

You'll be prompted to enter some additional information (e.g. mirror location) but it should be pretty easy to follow.

To run the script from the R console, simply type the following:

source('path_to_directory_containing_files/stochastic_multiple_sweeps_script.R')

where you should replace path_to_directory_containing_files with the actual path.

For example, if you placed the multiple_sweeps folder in your Downloads file then you should type (on linux/mac)

source('~/Downloads/multiple_sweeps/stochastic_multiple_sweeps_script.R')

** Note that the package doMC--the backend used to run parallel loops--is incompatible with windows and you will need to use the package doSNOW instead. If you replace all instances of doMC with doSNOW in the script then I think it will work without any other changes, but I have not tested this. **
